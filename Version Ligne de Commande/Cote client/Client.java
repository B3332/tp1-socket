package stream;

import java.io.*;
import java.net.*;

public class Client {

	public static void main(String[] args) throws IOException {

		Socket SocketClient = null;
		PrintStream socOut = null;
		BufferedReader stdIn = null;
		
		if (args.length != 2) {
			System.out.println("Mauvais passage des paramètres de connexion (host port)...");
			System.exit(1);
		}
		try {
			SocketClient = new Socket(args[0], new Integer(args[1]).intValue());
			socOut = new PrintStream(SocketClient.getOutputStream());
			stdIn = new BufferedReader(new InputStreamReader(System.in));
		} catch (UnknownHostException e) {
			System.err.println("Host inconnu:" + args[0]);
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for " + "the connection to:" + args[0]);
			System.exit(1);
		}

		// Reception

		ClientIn cIn = new ClientIn(SocketClient);
		cIn.start();

		// Gestion de l'envoi de message

		String line;

		while (true) {
			line = stdIn.readLine();
			socOut.println(line);
			if (line.equals("quit")){
				cIn.stopInput();
				break;
			}
			stdIn = new BufferedReader(new InputStreamReader(System.in));
		}
		socOut.close();
		stdIn.close();
		SocketClient.close();
	}

}
