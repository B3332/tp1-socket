package stream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ClientIn extends Thread {
	
	BufferedReader socIn = null;
	Socket SocketClient = null;
	boolean running = false;
	
	public ClientIn(Socket s){
		SocketClient = s;
		try {
			socIn = new BufferedReader(new InputStreamReader(SocketClient.getInputStream()));
			running = true;
		} catch (IOException e) {
			System.err.println("Problème de connexion...");
			System.exit(1);
		}
	}
	public void run(){
		while (running){
			try {
				System.out.println(socIn.readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void stopInput() throws IOException{
		running = false;
		socIn.close();
	}
}
