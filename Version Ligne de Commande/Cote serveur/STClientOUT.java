/***
 * STClientOUT
 * Example of a TCP server
 * Date: 25/11/2016
 * Authors: Vincent FALCONIERI
 */

package stream;

import java.io.*;
import java.net.*;

public class STClientOUT extends Thread {

	private Socket clientSocket;

	/** Constructeur qui récupère et stocke le Socket sur lequel il écoute/émet.
	 * @param s : le socket attribué au thread
	 */
	STClientOUT(Socket s) {
		this.clientSocket = s;
	}

	/**
	 * receives a request from client then sends an echo to the client
	 * 
	 * @param clientSocket the client socket
	 **/
	public void run() {
		try {
			
		} catch (Exception e) {
			System.err.println("Error in EchoServer:" + e);
		}
	}

}
