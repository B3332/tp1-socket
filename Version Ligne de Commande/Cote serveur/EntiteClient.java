package stream;

import java.io.BufferedReader;
import java.io.PrintStream;

public class EntiteClient {
	// Objets directement construits
	public String pseudo = "";
	public PrintStream fluxSortie = null;
	public BufferedReader fluxEntree = null;

	// Constructeur
	EntiteClient(String pseudo, PrintStream fluxSortie, BufferedReader fluxEntree) {
		this.pseudo = pseudo;
		this.fluxSortie = fluxSortie;
		this.fluxEntree = fluxEntree;
	}
}