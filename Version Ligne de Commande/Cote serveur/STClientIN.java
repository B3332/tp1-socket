/***
 * STClientIN
 * Example of a TCP server
 * Date: 25/11/2016
 * Authors: Vincent FALCONIERI
 */

package stream;

import java.io.*;
import java.net.*;

public class STClientIN extends Thread {

	private Socket clientSocket;
	private EntiteClient clientDeCeSocket;

	/**
	 * Constructeur qui récupère et stocke le Socket sur lequel il écoute/émet.
	 * 
	 * @param s
	 *            : le socket attribué au thread
	 */
	STClientIN(Socket s) {
		this.clientSocket = s;
	}

	/**
	 * receives a request from client then sends an echo to the client
	 * 
	 * @param clientSocket
	 *            : the client socket
	 **/
	public void run() {
		try {
			// On créé un flux d'entrée, qu'on lie avec le socket.
			BufferedReader socIn = null;
			socIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

			// On créé un flux de sortie, qu'on lie avec le socket.
			PrintStream socOut = new PrintStream(clientSocket.getOutputStream());

			//On créé les variables nécessaire pour la récupération du pseudo
			String joinEtPseudo = socIn.readLine();
			String[] arguments = joinEtPseudo.split(" ");

			//On récupère le pseudo du client
			if (arguments.length == 2 && arguments[0].equals("join")) {

				// On créé le client
				clientDeCeSocket = new EntiteClient(arguments[1], socOut, socIn);

				// On ajoute l'objet ListeEntiteClient au serveur
				ServeurMultiThreaded.listeEntiteClient.add(clientDeCeSocket);

				// On fait un affichage.
				socOut.println("Félicitation, vous vous êtes connecté.");

				// On lui fait récupérer l'historique des conversations
				ServeurMultiThreaded.casterHistorique(clientDeCeSocket);
				
				//on notifie de la connexion du nouvel utilisateur
				ServeurMultiThreaded.casterUnMessage("Serveur", arguments[1] + " s'est connecté");

			} else {
				socOut.println("Ciao.");
				// Renvoi une exception
				throw new Exception("Utilisateur ne veut pas faire 'join'.");
			}

			
			boolean estEnLigne = true;
			// On attend le besoin de l'émission.
			while (estEnLigne) {
				String line = socIn.readLine();

				switch (line) {
				case "quit":
					// On fait des affichages.
					socOut.println("A bientôt! Vous vous êtes déconnecté.");
					ServeurMultiThreaded.casterUnMessage("Serveur", arguments[1] + " s'est déconnecté");

					// On supprime l'user de la base.
					ServeurMultiThreaded.listeEntiteClient.remove(this.clientDeCeSocket);
					
					//On fermes les flux puis le socket
					this.clientDeCeSocket.fluxEntree.close();
					this.clientDeCeSocket.fluxSortie.close();
					clientSocket.close();
					
					//On casse la boucle
					estEnLigne = false;
					break;
				default:
					ServeurMultiThreaded.casterUnMessage(this.clientDeCeSocket.pseudo, line);
					break;
				}
			}
		} catch (Exception e) {
			System.err.println("Error in STClientIN:" + e);
		}
	}

}
