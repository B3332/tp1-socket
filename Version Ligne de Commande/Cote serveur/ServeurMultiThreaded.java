/***
 * ServeurMultiThreaded
 * Example of a TCP server
 * Date: 25/11/2016
 * Authors: Vincent FALCONIERI
 */

package stream;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ServeurMultiThreaded {

	/**
	 * Gère la liste des clients actuellement liés au serveur de chat.
	 */
	static ArrayList<EntiteClient> listeEntiteClient = new ArrayList<EntiteClient>();

	/**
	 * Gestion des écrivains / lecteurs de logs
	 */
	static BufferedWriter logWriter = null;
	static BufferedReader logReader = null;

	/**
	 * main method
	 * 
	 * @param port
	 *            : le numéro de port sur lequel le serveur écoutera les
	 *            requêtes de connexions clientes.
	 **/
	public static void main(String args[]) {
		ServerSocket listenSocket;

		if (args.length != 1) {
			System.out.println("Usage: java ServeurMultithreaded <EchoServer port>");
			System.exit(1);
		}

		try {
			// On créé le Socket serveur d'écoute pour les nouveaux clients.
			listenSocket = new ServerSocket(Integer.parseInt(args[0])); // port
			System.out.println("Server ready...");

			// On prépare le fichier de LOG
			logWriter = Files.newBufferedWriter(Paths.get("log.txt"), Charset.forName("UTF-8"));
			logReader = Files.newBufferedReader(Paths.get("log.txt"), Charset.forName("UTF-8"));

			// Attente d'un nouveau client
			while (true) {

				// Dès qu'une connexion est demandée
				Socket clientSocket = listenSocket.accept();
				System.out.println("Connexion from:" + clientSocket.getInetAddress());

				// On créé deux Thread uniquement pour ce client.
				STClientIN ctInput = new STClientIN(clientSocket);
				STClientOUT ctOutput = new STClientOUT(clientSocket);

				// On les stocke dans nos tableaux correspondants, statiques.
				// listeThreadClientIN.add(ctInput);
				// ServeurMultiThreaded.listeThreadClientOUT.add(ctOutput);

				// On le démarre
				ctInput.start();
				ctOutput.start();
			}

		} catch (Exception e) {
			System.err.println("Error in ServeurMultithreaded:" + e);
		}
	}

	/** Permet de broadcaster un message à tous les utilisateurs du chat
	 * @param pseudo : le pseudo qui émet le message
	 * @param message : le contenu du message à émettre
	 */
	public static void casterUnMessage(String pseudo, String message) {

		// On constitue la chaîne à Broadcast/mettre en historique/ etc.
		String txtDate = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.FRANCE).format(new Date());
		String tmpToBroadcast = txtDate + "| " + pseudo + "> " + message;

		// On écrit dans les logs
		try {
			logWriter.append(tmpToBroadcast); // On ajoute la ligne aux logs
			logWriter.newLine(); // Aller à la ligne suivante
			logWriter.flush(); // libère le buffer
		} catch (Exception e) {
			System.err.println("Error in ServeurMultithreaded:" + e);
		}

		// On broadcast le message à tout le monde en spécifiant le pseudo
		for (int i = 0; i < listeEntiteClient.size(); i++) {
			listeEntiteClient.get(i).fluxSortie.println(tmpToBroadcast);
		}
	}

	/** Permet de récupérer l'historique des conversations 
	 * @param entiteCible : la cible qui doit recevoir l'historique de conversation.
	 */
	public static void casterHistorique(EntiteClient entiteCible) {
		//NOTE : on pourrait limiter le nombre de lignes renvoyées .. mais ce n'est pas l'objet du TP. (Vision grande échelle)
		try {
			// Some greetings
			entiteCible.fluxSortie.println("== Récupération de l'historique de conversation ==");

			// On récupère tout le contenu
			String line = "";
			while ((line = logReader.readLine()) != null) {
				entiteCible.fluxSortie.println(line);
			}
			
			//On salut
			entiteCible.fluxSortie.println("== Fin de la récupération de l'historique de conversation ==");

			// Pour remettre à zéro.
			logReader.reset();
		} catch (Exception e) {
			System.err.println("Error in ServeurMultithreaded:" + e);
		}
	}
}

// // Lire fichier
// try(
//
// BufferedReader reader = Files.newBufferedReader(new
// Path("chemin/fichier.ext"), Charset.defaultCharset()))
// {
// String line = "";
// while ((line = reader.readLine()) != null) {
// System.out.println(line);
// }
//
// }catch(
// IOException ioe)
// {
// System.out.println("Erreur lors de la lecture");
// }
//
// // Ecrire
// try(
// BufferedWriter writer = Files.newBufferedWriter(new
// Path("chemin/fichier.ext"), Charset.defaultCharset()))
// {
// writer.append("Coucou");
// writer.newLine(); // Aller à la ligne suivante
// writer.append("Deuxième ligne");
// writer.flush(); // Si tu libère le buffer (ici ce n'est pas utile)
// }catch(
// IOException ioe)
// {
// System.out.println("Erreur écriture");
// }
