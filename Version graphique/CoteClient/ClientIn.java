package stream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientIn extends Thread {

    BufferedReader socIn = null;
    Socket SocketClient = null;
    boolean running = false;
    Client parent;

    public ClientIn(Client parent, Socket s) {
        this.parent = parent;
        SocketClient = s;
        try {
            socIn = new BufferedReader(new InputStreamReader(SocketClient.getInputStream()));
            running = true;
        } catch (IOException e) {
            System.err.println("Problème de connexion...");
            System.exit(1);
        }
    }

    public void run() {
        while (running) {
            try {
                parent.afficher(socIn.readLine());
            } catch (IOException ex) {
                Logger.getLogger(ClientIn.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void stopInput() throws IOException {
        running = false;
        socIn.close();
    }
}
