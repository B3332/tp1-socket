package clientchat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientIn extends Thread {

    BufferedReader socIn = null;
    Socket SocketClient = null;
    boolean running = false;
    InterfaceClient parent;

    /**
     * Constructeur de la classe Client In, hérité de thread. Ecoute le socket
     * et met à jour l'interface du client.
     *
     * @param parent: objet Client ayant appelé cette classe
     * @param s: socket client
     */
    public ClientIn(InterfaceClient parent, Socket s) {
        this.parent = parent;
        SocketClient = s;
        try {
            socIn = new BufferedReader(new InputStreamReader(SocketClient.getInputStream()));
            running = true;
        } catch (IOException e) {
            System.err.println("Problème de connexion...");
            System.exit(1);
        }
    }

    /**
     * Méthode run du thread, écoute et met à jour l'interface utilisateur
     */
    public void run() {
        while (running) {
            try {
                parent.afficher(socIn.readLine());
            } catch (IOException ex) {
                Logger.getLogger(ClientIn.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Méthode utilisée lors de l'arrêt du thread
     *
     * @throws IOException : peut déclencher une erreur d'entrée sortie à cause du socket
     */
    public void stopInput() throws IOException {
        running = false;
        socIn.close();
    }
}
