package serverchat;

import java.io.*;
import java.net.*;
import javax.swing.event.EventListenerList;

/**
 * @author vfalconieri Classe qui gère les inputs envoyées par le client,
 * jusqu'au serveur. Gère la récupération du pseudo, de la deconnexion du
 * serveur ...
 */
public class E_ThreadWaitClientInput extends Thread {

    private Socket clientSocket;
    private M_entiteClient clientDeCeSocket;
    M_MainThread threadParent;

    //Ses propres évènements, un seul objet pour tous les types d'écouteurs
    private final EventListenerList listeners = new EventListenerList();

    /**
     * Constructeur qui récupère et stocke le Socket sur lequel il écoute/émet.
     *
     * @param s : le socket attribué au thread
     * @param threadParent : le thread qui a invoqué cet objet (thread lui
     * aussi)
     */
    E_ThreadWaitClientInput(Socket s, M_MainThread threadParent) {
        this.clientSocket = s;
        this.threadParent = threadParent;
    }

    /**
     * Thread principal de la classe, qui attend des messages entrants de la
     * part du client.
	 *
     */
    public void run() {
        try {
            // On créé un flux d'entrée, qu'on lie avec le socket.
            BufferedReader socIn = null;
            socIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            // On créé un flux de sortie, qu'on lie avec le socket.
            PrintStream socOut = new PrintStream(clientSocket.getOutputStream());

            //On créé les variables nécessaire pour la récupération du pseudo
            String joinEtPseudo = socIn.readLine();
            String[] arguments = joinEtPseudo.split(" ");

            //On récupère le pseudo du client
            if (arguments.length == 2 && arguments[0].equals("join")) {

                // On créé le client
                clientDeCeSocket = new M_entiteClient(arguments[1], socOut, socIn);

                // On ajoute l'objet ListeEntiteClient au serveur
                M_MainThread.listeEntiteClient.add(clientDeCeSocket);

                // On fait un affichage.
                socOut.println("Félicitation, vous vous êtes connecté.");

                // On lui fait récupérer l'historique des conversations
                M_MainThread.casterHistorique(clientDeCeSocket);

                //On lève un event dans le parent.
                //On averti le parent
                this.firechatConnexionClient(arguments[1] + " s'est connecté");
                
                //on notifie de la connexion du nouvel utilisateur
                M_MainThread.casterUnMessage("Serveur", arguments[1] + " s'est connecté");

            } else {
                socOut.println("Ciao.");
                // Renvoi une exception
                throw new Exception("Utilisateur ne veut pas faire 'join'.");
            }

            boolean estEnLigne = true;
            // On attend le besoin de l'émission.
            while (estEnLigne) {
                String line = socIn.readLine();

                switch (line) {
                    case "quit":
                        // On fait des affichages.
                        socOut.println("A bientôt! Vous vous êtes déconnecté.");
                        M_MainThread.casterUnMessage("Serveur", arguments[1] + " s'est déconnecté");

                        // On supprime l'user de la base.
                        M_MainThread.listeEntiteClient.remove(this.clientDeCeSocket);
                        
                        //On averti le parent
                        this.firechatDeconnexionClient(arguments[1] + " s'est deconnecté");
                
                        //On fermes les flux puis le socket
                        this.clientDeCeSocket.fluxEntree.close();
                        this.clientDeCeSocket.fluxSortie.close();
                        clientSocket.close();

                        //On casse la boucle
                        estEnLigne = false;
                        break;
                    default:
                        M_MainThread.casterUnMessage(this.clientDeCeSocket.pseudo, line);
                        break;
                }
            }
        } catch (Exception e) {
            System.err.println("Error in STClientIN:" + e);
        }
    }

    public void addChatListener(E_ChatListener listener) {
        listeners.add(E_ChatListener.class, listener);
    }

    public void removeChatListener(E_ChatListener listener) {
        listeners.remove(E_ChatListener.class, listener);
    }

    /**
     * @return : la liste des listeners de cette classe
     */
    public E_ChatListener[] getChatListener() {
        return listeners.getListeners(E_ChatListener.class);
    }

    /**
     * Permet de notifier les écouteurs de la connexion d'un client
     *
     * @param message : Le message à transférer aux écouteurs
     */
    protected void firechatConnexionClient(String message) {
        E_ChatEvent e = new E_ChatEvent(message);

        for (E_ChatListener listener : getChatListener()) {
            listener.chatConnexionClient(e);
        }
    }

    /**
     * Permet de notifier les écouteurs de la déconnexion d'un client
     *
     * @param message : Le message à transférer aux écouteurs
     */
    protected void firechatDeconnexionClient(String message) {
        E_ChatEvent e = new E_ChatEvent(message);

        for (E_ChatListener listener : getChatListener()) {
            listener.chatDeconnexionClient(e);
        }

    }

    /**
     * Permet de notifier les écouteurs d'un texte que le serveur veut délivrer
     *
     * @param message : Le message à transférer aux écouteurs
     */
    protected void firechatTexteServeur(String message) {
        E_ChatEvent e = new E_ChatEvent(message);

        for (E_ChatListener listener : getChatListener()) {
            listener.chatTexteServeur(e);
        }
    }
}
