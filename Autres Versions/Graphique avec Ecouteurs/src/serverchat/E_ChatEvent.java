package serverchat;

/**
 * Un évènement qui peut être envoyé par le chat. Contient par exemple un message, suivant l'évènement.
 * @author Vincent FALCONIERI
 */
public class E_ChatEvent {
 
    private String textEvent;
 
    E_ChatEvent(String textEvent) {
        this.textEvent = textEvent;
    }
 
    public String getTextEvent() {
        return textEvent;
    }
 
}
