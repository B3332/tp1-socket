package serverchat;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import javax.swing.SwingUtilities;
import javax.swing.event.EventListenerList;

/**
 * Thread principal du serveur qui attend les nouvelles connexion, pour des
 * clients voulants joindre le serveur.
 *
 * @author Vincent FALCONIERI
 */
public class M_MainThread extends Thread {

    /**
     * Gère la liste des clients actuellement liés au serveur de chat.
     */
    static ArrayList<M_entiteClient> listeEntiteClient = new ArrayList<M_entiteClient>();

    /**
     * Gestion des écrivains / lecteurs de logs
     */
    static BufferedWriter logWriter = null;
    static BufferedReader logReader = null;

    // Variables utiles
    ServerSocket listenSocket;
    static String hostname;
    static int PORT;
    boolean running = true;

    //Gestion des évènements, un seul objet pour tous les types d'écouteurs
    private final EventListenerList listeners = new EventListenerList();

    public M_MainThread(String host, int portEcoute) {
        this.hostname = hostname;
        this.PORT = portEcoute;
    }

    /**
     * Permet de lancer l'écoute du serveur sur le port d'écoute, en attente de client se connectant pour la première fois.
     */
    public void run() {
        try {
            // On créé le Socket serveur d'écoute pour les nouveaux clients.
            listenSocket = new ServerSocket(PORT);
            System.out.println("Server ready...");

            // On prépare le fichier de LOG
            logWriter = Files.newBufferedWriter(Paths.get("log.txt"), Charset.forName("UTF-8"));
            logReader = Files.newBufferedReader(Paths.get("log.txt"), Charset.forName("UTF-8"));

            // Attente d'un nouveau client
            while (running) {

                // Dès qu'une connexion est demandée
                Socket clientSocket = listenSocket.accept();
                System.out.println("Connexion from:" + clientSocket.getInetAddress());

                // On créé un Thread uniquement pour ce client.
                E_ThreadWaitClientInput threadClientInput = new E_ThreadWaitClientInput(clientSocket, this);

                // Gestion des évènements
                threadClientInput.addChatListener(new E_ChatAdapter() {
                    public void chatConnexionClient(E_ChatEvent e) {
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                //On fait remonter la patate chaude
                                for (E_ChatListener listener : getChatListener()) {
                                    listener.chatConnexionClient(e);
                                }
                                System.out.println("remonter la patate chaude");
                            }
                        });
                    }

                    public void chatDeconnexionClient(E_ChatEvent e) {
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                //On fait remonter la patate chaude
                                for (E_ChatListener listener : getChatListener()) {
                                    listener.chatDeconnexionClient(e);
                                }
                            }
                        });
                    }

                    public void chatTexteServeur(E_ChatEvent e) {
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                // à chaque fois que le serveur veut afficher quelques chose, on l'affiche directement sur la console, pour l'instant
                                for (E_ChatListener listener : getChatListener()) {
                                    listener.chatTexteServeur(e);
                                }
                            }
                        });
                    }

                });
                // On le démarre
                threadClientInput.start();
            }

        } catch (Exception e) {
            System.err.println("Error in ServeurMultithreaded:" + e);
        }
    }

    /**
     * Permet de broadcaster un message à tous les utilisateurs du chat
     *
     * @param pseudo : le pseudo qui émet le message
     * @param message : le contenu du message à émettre
     */
    public static void casterUnMessage(String pseudo, String message) {

        // On constitue la chaîne à Broadcast/mettre en historique/ etc.
        String txtDate = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.FRANCE).format(new Date());
        String tmpToBroadcast = txtDate + "| " + pseudo + "> " + message;

        // On écrit dans les logs
        try {
            logWriter.append(tmpToBroadcast); // On ajoute la ligne aux logs
            logWriter.newLine(); // Aller à la ligne suivante
            logWriter.flush(); // libère le buffer
        } catch (Exception e) {
            System.err.println("Error in M_MainThread :" + e);
        }

        // On broadcast le message à tout le monde en spécifiant le pseudo
        for (int i = 0; i < listeEntiteClient.size(); i++) {
            listeEntiteClient.get(i).fluxSortie.println(tmpToBroadcast);
        }
    }

    /**
     * Permet de récupérer l'historique des conversations
     *
     * @param entiteCible : la cible qui doit recevoir l'historique de
     * conversation.
     */
    public static void casterHistorique(M_entiteClient entiteCible) {
        //NOTE : on pourrait limiter le nombre de lignes renvoyées .. mais ce n'est pas l'objet du TP. (Vision grande échelle)
        try {
            // Some greetings
            entiteCible.fluxSortie.println("== Récupération de l'historique de conversation ==");

            // On récupère tout le contenu
            String line = "";
            while ((line = logReader.readLine()) != null) {
                entiteCible.fluxSortie.println(line);
            }

            //On salut
            entiteCible.fluxSortie.println("== Fin de la récupération de l'historique de conversation ==");

        } catch (Exception e) {
            System.err.println("Error in ServeurMultithreaded:" + e);
        }
    }

    public void addChatListener(E_ChatListener listener) {
        listeners.add(E_ChatListener.class, listener);
    }

    public void removeChatListener(E_ChatListener listener) {
        listeners.remove(E_ChatListener.class, listener);
    }

    /**
     * @return : la liste des listeners de cette classe
     */
    public E_ChatListener[] getChatListener() {
        return listeners.getListeners(E_ChatListener.class);
    }

    /**
     * Permet de notifier les écouteurs de la connexion d'un client
     *
     * @param message : Le message à transférer aux écouteurs
     */
    protected void firechatConnexionClient(String message) {
        E_ChatEvent e = new E_ChatEvent(message);

        for (E_ChatListener listener : getChatListener()) {
            listener.chatConnexionClient(e);
        }
    }

    /**
     * Permet de notifier les écouteurs de la déconnexion d'un client
     *
     * @param message : Le message à transférer aux écouteurs
     */
    protected void firechatDeconnexionClient(String message) {
        E_ChatEvent e = new E_ChatEvent(message);

        for (E_ChatListener listener : getChatListener()) {
            listener.chatDeconnexionClient(e);
        }

    }

    /**
     * Permet de notifier les écouteurs d'un texte que le serveur veut délivrer
     *
     * @param message : Le message à transférer aux écouteurs
     */
    protected void firechatTexteServeur(String message) {
        E_ChatEvent e = new E_ChatEvent(message);

        for (E_ChatListener listener : getChatListener()) {
            listener.chatTexteServeur(e);
        }
    }

    /**
     * Permet d'arrêter, presque, proprement le main thread du serveur.
     */
    public void stopMainThread() {
        //Il faudrait faire plus de manipulations = faire la liste des thread, et les killer un par un.
        this.casterUnMessage("SERVEUR", "Le serveur s'arrête ... veuillez vous déconnecter");

        this.running = false;

    }

}
