package serverchat;

import java.util.EventListener;

/**
 * Les listeners qui doivent être implémentés si on veut écouter un "Serveur de chat"
 * @author Vincent FALCONIERI
 */
public interface E_ChatListener extends EventListener  {
	void chatConnexionClient(E_ChatEvent e);
	void chatDeconnexionClient(E_ChatEvent e);
	void chatTexteServeur(E_ChatEvent e);
}
