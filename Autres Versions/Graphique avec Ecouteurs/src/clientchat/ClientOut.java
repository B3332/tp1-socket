package clientchat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

/**
 *
 * @author Corentin
 */
public class ClientOut {
    PrintStream socOut = null;
    Socket SocketClient = null;
    Client parent;
    
    public ClientOut(Client parent, Socket s) throws IOException{
        this.parent = parent;
        SocketClient = s;
        socOut = new PrintStream(SocketClient.getOutputStream());
    }
    void envoyer(String s){
        socOut.println(s);
    }
}
